AtlasACM
==========================

ATLAS package management and compilation software for cmake and git.

<b>Please signup to our mailing list: <a href=mailto:atlas-sw-acm-users@cern.ch>atlas-sw-acm-users@cern.ch</a> - ask any questions you have about acm to this list too</b>

How to checkout/install
----------------------

```acm``` is now available once you do ```setupATLAS```! 
For developers of acm, you can checkout the latest version, and are advised to set it up as a dev version. See the section at the bottom of this page if you need more information.

Cheat sheet
----------------------

### setup release with empty source area
```
mkdir build source
cd build
acmSetup AnalysisBase,21.2.4
```

Sets up ```AnalysisBase,21.2.4``` with your ```$SourceArea``` equal to the source directory. A project ```CMakeLists.txt``` file is created and the project is configured and setup.

### setup release using existing project as source area
```
mkdir build
cd build
acmSetup --sourcerepo=will/demo --sourcebranch=CamJetSelector AthAnalysis,21.2.14
```
clones the ```CamJetSelector``` branch of the ```https://gitlab.cern.ch/will/demo``` repository and uses that as the source directory. Configures and sets it up.

### sparse clone your athena fork
```
acm sparse_clone_project athena <gitlabUserName>/athena
```

creates a sparse clone of the ```https://gitlab.cern.ch/<gitlabUserName>/athena``` repository into your ```$SourceArea```. The clone is automatically put into the same branch/tag as you have set up

### add a package from a cloned project to the compilation
```
acm add_pkg athena/Control/AthenaCommon
```

Adds the ```Control/AthenaCommon``` package to compilation (updates the ```package_filters.txt``` file).

### compile everything
```
acm compile
```

compiles everything.

### clone an entire project and add all its packages to compilation
```
acm clone_project will/AnalysisCam
acm add_pkg AnalysisCam/.\*
```

Clones the  ```https://gitlab.cern.ch/will/AnalysisCam``` project into your ```$SourceArea``` and updates your ```package_filters.txt``` so that all the packages in it will be compiled.

### exclude a package from compilation
```
acm exclude_pkg AnalysisCam/AnalysisCamExample
```

Excludes the given package of the project from compilation

### run tests of a given package

```
acm test MyPackage
```

Runs any ctests available for ```MyPackage```


### Full list of acm commands

`acmSetup` is the command that *wraps* over the top of `asetup`. It takes care of the following things for you:

  * Calling `asetup` command
  * Creating a `CMakeLists.txt` file for your source directory if one does not exist
  * Sourcing the `setup.sh` script of your build directory
  * If no `setup.sh` script exists, a cmake configure is performed on the source directory and the resulting `setup.sh` script is sourced 


You should call `acmSetup` inside your `build` directory.

```
acmSetup [--sourcearea=<sourcepath>] [--sourcerepo=<sourcerepo>] [--sourcebranch=<sourcebranch/tag>] <asetupOptions>
```

If `--sourcearea` is specified, use given path as the location of source code. Default value is `../source`.

If `--sourcerepo` is specified, will clone the given git repo and use that as the source area. Cloning is done at the parent directory level. `--sourcebranch` option can be used to specify the branch or tag that should be cloned.

If the `sourcearea` does not point to a valid location, `acmSetup` will still execute `asetup` but you *will not have the acm commands available* after this. This is called *sourceless* setup.



You are recommended to use ```acm -h``` to list all the commands available to you, and you can use ```acm <command> -h``` to get more information about any given command.

| Command | Purpose/Description |
|---------|---------------------|
| `find_packages` | reconfigures your project, ensuring cmake knows about all packages. This command will wipe and recreate the CMakeCache. |
| `compile` | builds your project |
| `compile_pkg <pkgName>` | compile a single package |
| `test <pkgName>` | run the cmake tests for a package |
| `clean [-f]` | does a cmake clean. if `-f` included, will also do a `find_packages`. Use this command after switching releases |
| `clone_project [name] <path> [branch/tag]` | clone a git project, adding it as a sub-project to your project, e.g. `clone_project will/TRooFit`. If no name is given, the name is taken from the path. |
| `sparse_clone_project [name] <path>` | sparse-clone a project. Optional shorthand for `athena`, just do `sparse_clone_project athena`. acm will put the athena project in the same state (branch/tag) as your setup release |
| `add_pkg <pkgPath>` | Include a package (or packages if wildcards e.g. `athena/Control/\.*`) in the compilation. |
| `exclude_pkg <pkgPath>` | Exclude a package/packages from the compilation |
| `add_pkg_clients <pkgPath>` | Adds all packages that are clients of the specified package |
| `switch <branch/tag> <pkgPath>` | Move a package's state to a different branch/tag to its project's branch/tag |
| `new_pkg <pkgName>` | Creates a new package in your project with the given name |
| `new_skeleton <pkgName>` | Creates a skeleton analysis package (package with an algorithm and a joboption). After compilation, you will be able to run it with `athena --filesInput=path/to/xAOD.root <pkgName>/<pkgName>AlgJobOptions.py` |

A complete example
----------------------
More detail is provided in the sections below, but in this example we will use ```acm``` to:
  * set up a release, 
  * checkout the ```Control/AthAnalysisBaseComps``` package from the release (using sparse-checkout), 
  * create a skeleton ```MyPackage``` that contains a skeleton algorithm ```MyPackageAlg``` ready to run
  * compile everything
  * run that algorithm

(Note that the ```sparse_clone_project``` command will assume your machine username is the same as your gitlab username, if its different you specify your gitlab username as soon)

```
mkdir source build run
cd build
acmSetup --sourcearea=../source AthAnalysis,21.2,latest
acm sparse_clone_project athena [gitusername]/athena
acm add_pkg athena/Control/AthAnalysisBaseComps
acm new_skeleton MyPackage
acm compile
cd ../run
athena MyPackage/MyPackageAlgJobOptions.py
```

How to setup a release with ACM
----------------------
The two things ACM needs are:

1. The path to your ```SourceArea```: This is where your code lives. Specify with the ```--sourcearea``` argument
2. The path to your ```TestArea```: This is where your code is compiled - usually will be the directory you call acmSetup inside

All other arguments will be passed onto the ```asetup``` command. Essentially you should think of ```acmSetup``` like an extended version of ```asetup```, where you also specify the path to your source code

Assuming You have created the alias above:

    mkdir source build
    cd build
    acmSetup --sourcearea=../source AthAnalysis,21.2.0
    acm -h

I.e. the norm is that you should acmSetup inside the build directory (aka your ```TestArea```). To un-setup:

    acmSetup --unset

In future terminals, you can then simply go do:

```
cd build
acmSetup
```

and it will set up whatever you had before.

In acm version 0.1.2 onwards, you will also be able to specify a git repository as your source repository. For example:

```
mkdir build
cd build
acmSetup --sourcerepo=will/demo AthAnalysis,21.2.5
```

The above will clone the gitlab repository https://gitlab.cern.ch/will/demo into a directory called ```demo``` in the directory above where you call acmSetup. It will then make that directory the source directory. If you want to use a different name for the directory you can do:

```
acmSetup --sourcerepo=will/demo --sourcearea=../source AthAnalysis,21.2.5
```

will clone the repo into ```../source``` instead.

Getting started (a skeleton package)
----------------------
You can create a skeleton Package (in AthAnalysis only) with:

```
acm new_skeleton MyPackage
```

If you write your own packages, you can tell cmake about them with just:

```
acm find_packages
```

which is identical to ```cmake -DPACKAGE_FILTER_FILE=$TestArea/package_filters.txt $TestArea```.

Compiling code
----------------------
Just do:

```
acm compile
```

which is equivalent to ```cmake --build $TestArea```


Cloning projects and compiling with sparse build (e.g. compling athena code)
-----------------------
You can add the ```athena``` project to your source directory like this:

```
acm clone_project athena <url_to_your_athena_fork>
```

In fact, athena is treated as a special case for a project ... it will also add the central repo as an upstream remote, and also checkout the same version of the project that you set up earlier (with acmSetup)

By default, when you clone projects into your ```$SourceArea```, all packages of that project will be filtered out of the compilation. To add a package to the compilation you do:

```
acm add_pkg athena/Path/To/SomePackage
```

This is ultimately just modifying the content of the ```package_filters.txt``` file in your ```$TestArea```.

You can also remove a package with:

```
acm rm_pkg athena/Path/To/SomePackage
```

Sparse Cloning projects 
-----------------------
For a large project like ```athena```, a clone of the entire project can take a long time as well as disk space, especially if you only want to compile a few packages in the project.

For this, we provide ```sparse_clone_project```, which you use like this:

```
acm sparse_clone_project athena
```

<b>Note: ```sparse_clone_project``` has only been tested with the ```athena``` project - any attempt to use other projects may not work<.b>

Note that only a small part of the athena project is checked out by this command (the ```Projects``` directory. In fact, ideally this wouldn't even be checked out, but is a limitation at the moment)

After this you just call ```add_pkg``` like above, e.g:

```
acm add_pkg athena/Control/AthAnalysisBaseComps
```

This should cause the package to be checked out and appear in the ```athena``` directory. Then you can compile the code as usual with ```acm compile```

Checkout different version of package
------------------------
If you have been given a specific hash or branch or tag version of a package or packages you want to test, you can move those individual packages onto that hash by doing:

```
acm switch <hash> athena/Path/To/SomePackage
```

For example, to switch the ```Control/AthAnalysisBaseComps``` package to the version in the nightly branch you can do:

```
acm switch upstream/21.2 athena/Control/AthAnalysisBaseComps
```

To switch to the version in tag ```21.2.0``` you can do:

```
acm switch tags/release/21.2.0 athena/Control/AthAnalysisBaseComps
```


Checking out development versions of acm
----------------------

These instructions are for developer's benefits:

```
mkdir ~/acmDev
cd ~/acmDev
git clone https://:@gitlab.cern.ch:8443/atlas-sit/acm.git
```

You are then need to ensure the following two lines are always called before you try to use acmSetup ... so add them to you ```~/.bashrc``` and/or ```~/.zshenv``` files

```
alias acmSetupDev='source ~/acmDev/acm/acmSetup.sh'
```

Then to use your checked out version, just replace ```acmSetup``` with ```acmSetupDev``` in your setup commands.


List of affecting environment variables
-----------------------
ACM_GITLAB_PATH_PREFIX - used as the path prefix for git commands. If unspecified we use the default: https://:@gitlab.cern.ch:8443/
<br>ReleaseArea - when running local setup (away from ALRB) then this will be used in place of --releasearea option
<br>ReleaseSetupScript - if specified and running local setup (away from ALRB) then this script will be sourced to setup any requested release! This was needed by docker setup
